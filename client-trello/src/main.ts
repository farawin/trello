import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import folan from './folan'
import './style/index.scss'

console.log(folan)

const app = createApp(App)

app.use(router)

app.directive('autofocus', {
  mounted(el) {
    el.focus()
  },
})

app.mount('#app')
