
/**
 * این تابع دو عدد را جمع میزند
 * @param {number} a عدد اول
 * @param {number} b عدد دوم
 * @returns {number} حاصل جمع
 */
function add(a, b) {
  return a + b
}

module.exports = {
  name: 'hassan',
}
