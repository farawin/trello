import { get, post } from '@/utils/http'
import { readonly, ref } from 'vue'

const _boards = ref([] as fw.Board[])

export const boards = readonly(_boards)

console.log(_boards)

export function fetchBoards(): void {
  get('/getuserboards').then(response => {
    if (response.code === 6 /* Found no board for this user */) {
      _boards.value = []
    } else {
      _boards.value = response.boards
    }
  })
}

export function addBoard(board: fw.Board) {
  post('/board', board).then(response => {
    if (response.success) {
      _boards.value.push(response.board)
    }
  })
}

// addBoard({
//   name: 'ToDo',
//   color: '#ff0000',
//   icon: 'backend',
//   members: [],
// })
