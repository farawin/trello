import { computed, readonly, ref, watch } from 'vue'

const _token = ref(localStorage.getItem('token') || '')

watch(_token, (token) => {
  localStorage.setItem('token', token)
})

export function setToken(value: string) {
  _token.value = value
}

export const token = readonly(_token)

export const isLogin = computed(() => !!_token.value)
