import router from '@/router'
import { setToken, token } from '@/models/auth'

type RequestParameters = [method: 'GET', url: string] | [method: 'POST', url: string, data: any]

export function request(...parameters: RequestParameters) {
  const [method, url] = parameters
  const data = parameters[2]

  return new Promise<any>((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.open(method, `/api${url}`)
    xhr.setRequestHeader('Content-Type', 'application/json')
    if (token.value) {
      xhr.setRequestHeader('access_token', token.value)
    }
    xhr.onload = async () => {
      try {
        const result = JSON.parse(xhr.responseText)
        if (result.code === 1 /* Access token expired */) {
          try {
            const result = await request('POST', '/refreshtoken', { refreshToken: localStorage.getItem('refreshToken') })
            setToken(result.access_token)
            return request(method as any, url, data)
          } catch (err) {
            router.push('/login')
          }
        }
        resolve(result)
      } catch (err) {
        reject(err)
      }
    }
    xhr.onerror = reject
    xhr.send(JSON.stringify(data))
  })
}

export function post(url: string, data: any) {
  return request('POST', url, data)
}

export function get(url: string) {
  return request('GET', url)
}
