declare global {
  namespace fw {
    type Board = {
      name: string;
      color: string;
      icon: string;
      members: string[];
    }
  }
}

export default fw.Board
